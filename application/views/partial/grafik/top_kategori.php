<div class="panel panel-inverse" data-sortable-id="index-1">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Barang Terjual</h4>
    </div>
    <div class="panel-body">
        <table class="table table-bordered table-striped">
            <tr><th colspan="2">Paling Banyak</th></tr>
            <?php foreach ($topSell as $item):?>
                <tr><td><?=$item->namabarang?></td><td><?=$item->qty?></td></tr>
            <?php endforeach;?>
        </table>
        <hr>
        <table class="table table-bordered table-striped">
            <tr><th colspan="2">Paling Sedikit</th></tr>
            <?php foreach ($bottomSell as $item):?>
                <tr><td><?=$item->namabarang?></td><td><?=$item->qty?></td></tr>
            <?php endforeach;?>
        </table>
    </div>
</div>