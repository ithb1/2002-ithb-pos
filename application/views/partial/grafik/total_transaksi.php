<?php
function bulan_terbilang($bulan){
    switch ($bulan){
        case 1: $bulanStr = "Januari"; break;
        case 2: $bulanStr = "Februari"; break;
        case 3: $bulanStr = "Maret"; break;
        case 4: $bulanStr = "April"; break;
        case 5: $bulanStr = "Mei"; break;
        case 6: $bulanStr = "Juni"; break;
        case 7: $bulanStr = "Juli"; break;
        case 8: $bulanStr = "Agustus"; break;
        case 9: $bulanStr = "September"; break;
        case 10: $bulanStr = "Oktober"; break;
        case 11: $bulanStr = "November"; break;
        case 12: $bulanStr = "Desember"; break;
        default: $bulanStr = ""; break;
    }
        return $bulanStr;

}
?>









            <!-- begin row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse" data-sortable-id="chart-js-1">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Line Chart</h4>
                        </div>
                        <div class="panel-body">
                            <p>
                                A line chart is a way of plotting data points on a line.
                                Often, it is used to show trend data, and the comparison of two data sets.
                            </p>
                            <div>
                                <canvas id="line-chart" style="height: 275px !important; width: 100%"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            


    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <!-- ================== END PAGE LEVEL JS ================== -->
  
  <script>
    $(document).ready(function() {

    });
  </script>