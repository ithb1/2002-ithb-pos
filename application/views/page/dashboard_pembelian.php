<div class="row">
    <section class="col-lg-12">
        <?php $this->load->view("partial/grafik/total_transaksi");?>
    </section>
</div>
<div class="row">
    <section class="col-lg-3">
        <div class="row">
            <div class="col-lg-12">
                <?php $this->load->view("partial/grafik/faktur_pembelian");?>
            </div>
            <div class="col-lg-12">
                <?php $this->load->view("partial/grafik/total_pembelian");?>
            </div>
        </div>
    </section>
    <section class="col-lg-6">
        <?php $this->load->view("partial/grafik/total_transaksi_weekly");?>
    </section>
    <section class="col-lg-3">
        <?php $this->load->view("partial/grafik/top_kategori");?>
    </section>
</div>