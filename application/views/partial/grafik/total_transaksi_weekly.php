<div class="panel panel-inverse" data-sortable-id="index-1">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">
            Weekly Transaction (dari
            <?=($tanggalFilter) ? date("d-m-Y", strtotime($tanggalFilter."-6 day")) : date("d-m-Y",strtotime(date("Y-m-d")."-7 day"))?>
            sampai tgl
            <?=($tanggalFilter) ? date("d-m-Y", strtotime($tanggalFilter)) : date("d-m-Y",strtotime(date("Y-m-d")."-1 day"))?>)
        </h4>
    </div>
    <div class="panel-body">

        <?=form_open("Welcome","class='form-horizontal'");?>
        <span class="row">
            <span class="col-md-3">
                <input type="date" name="tanggalFilter" class="form-control" value="">
            </span>
            <span class="col-md-1">
                <?=form_submit("btnsubmit", "filter","class='btn btn-default'");?>
            </span>
        </span>
        <?=form_close();?>

                                <canvas id="bar-chart"></canvas>
    </div>
</div>