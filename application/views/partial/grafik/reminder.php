<?php
if($rowKrisis):
    foreach ($rowKrisis as $row):?>
    <div class="alert alert-danger fade in m-b-15">
        Stok <strong><?=$row['nama']?></strong> tersisa <strong><?=$row['sisa']?></strong>. Segera melakukan restock.
        <span class="close" data-dismiss="alert">×</span>
    </div>
    <?php endforeach;
endif;?>