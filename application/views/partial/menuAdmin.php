<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Home</span> </a></li>
  <li class="<?=($this->uri->segment(2) == 'Kategori')?'active':''?>"><a href="<?=site_url('Master/Kategori')?>"><i class="fa fa-tag"></i> <span>Kategori</span></a></li>
  <li class="<?=($this->uri->segment(2) == 'Barang')?'active':''?>"><a href="<?=site_url('Master/Barang')?>"><i class="fa fa-archive"></i> <span>Barang</span></a></li>
  <li class="<?=($this->uri->segment(2) == 'Supplier')?'active':''?>"><a href="<?=site_url('Master/Supplier')?>"><i class="fa fa-user-circle"></i> <span>Supplier</span></a></li>
  <li class="<?=($this->uri->segment(2) == 'User')?'active':''?>"><a href="<?=site_url('Master/User')?>"><i class="fa fa-users"></i> <span>User</span></a></li>
  <li class="<?=($this->uri->segment(1) == 'Pembelian')?'active':''?>"><a href="<?=site_url('Pembelian')?>"><i class="fa fa-shopping-basket"></i> <span>Pembelian</span></a></li>
  <li class="<?=($this->uri->segment(1) == 'Penjualan')?'active':''?>"><a href="<?=site_url('Penjualan')?>"><i class="fa fa-shopping-cart"></i> <span>Penjualan</span></a></li>
    <li class="treeview <?=($this->uri->segment(1) == 'Laporan')?'active':''?>">
        <a href="#">
            <i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
            <li class="<?=($this->uri->segment(2) == 'LPenjualan')?'active':''?>"><a href="<?=site_url('Laporan/LPenjualan')?>"><span>Penjualan</span></a></li>
            <li class="<?=($this->uri->segment(2) == 'LPembelian')?'active':''?>"><a href="<?=site_url('Laporan/LPembelian')?>"><span>Pembelian</span></a></li>
            <li class="<?=($this->uri->segment(2) == 'LBarang')?'active':''?>"><a href="<?=site_url('Laporan/LBarang')?>"><span>Barang</span></a></li>
        </ul>
    </li>
</ul>