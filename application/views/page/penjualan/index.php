
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Penjualan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=""> Penjualan </a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Tabel Penjualan
                    </h3>
                    <div class="pull-right">
                        <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalForm" onclick="clearForm()"><i class="fa fa-plus"></i> Tambah Penjualan</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Faktur</th>
                            <th>Kasir</th>
                            <th>Nama Pembeli</th>
                            <th>No Telp Pembeli</th>
                            <th>Tanggal Faktur</th>
                            <th>Total</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($rowData as $row) :
                            ?>
                            <tr>
                                <td><?=$no++;?></td>
                                <td><?='#'.$row->nomorfaktur;?></td>
                                <td><?=$this->M_user->getDetail($row->userid)->fullname;?></td>
                                <td><?=$row->nama;?></td>
                                <td><?=$row->notelp;?></td>
                                <td><?=date("d-m-Y",strtotime($row->tanggal_faktur));?></td>
                                <td><?=number_format($row->total,0,",",".");?></td>
                                <td>
                                    <a href="<?=site_url('Penjualan/detail/'.$row->id);?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                    <a href="<?=site_url('Penjualan/delete/'.$row->id);?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Penjualan</h4>
            </div>
            <?=form_open("Penjualan/add","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
<!--                            ANDA YAKIN MENAMBAH ORDER?-->
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label">Nama Pembeli</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="notelp" class="col-sm-4 control-label">No Telepon Pembeli</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="notelp" placeholder="notelp" name="notelp" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "ya","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="modalTanggal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <?=form_open("Penjualan/addTanggal","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal" class="col-sm-4 control-label">Tanggal Datang</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                                    <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal_datang" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Penjualan/detailJson/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('#id').val(id).hide();
                $('#nama').val(data.nama);
                $('#notelp').val(data.notelp);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#nama').val("");
        $('#notelp').val("");
    }

    function tanggal(ini) {
        var id = $(ini).attr('data-id');
        $('.id').val(id);
    }
</script>