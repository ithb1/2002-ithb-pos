<div class="widget widget-stats bg-green">
	<div class="stats-icon"><i class="fa fa-shopping-cart"></i></div>
	<div class="stats-info">
		<h4><b>Total Transaksi Penjualan</b> -<?=date("M Y", strtotime($bulanFilter))?>-</h4>
		<p><?="$totalFakturPenjualan / $target";?><small style="color: white"><?=" (".(($totalFakturPenjualan/$target) * 100)." %)"?></small> </p>	
	</div>
</div>
