<?php
$rowBarang = $this->M_mst_barang->getAll();
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Penjualan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=site_url('Laporan/LPenjualan');?>"> Penjualan </a></li>
        <li><a href=""> Penjualan #<?=$data->nomorfaktur?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <?php if($this->session->flashdata("warning")): ?>
                        <div class="col-md-12">
                            <div class="alert alert-warning" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <?=$this->session->flashdata("warning")?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <h3 class="box-title">
                        Tabel Data FAKTUR <?=$data->nomorfaktur?>
                    </h3>
                    <div class="pull-right">
                        <a href="#" class="btn btn-xs btn-success"><i class="fa fa-print"></i> Print</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($rowData as $row) :
                            ?>
                            <tr>
                                <td><?=$no++;?></td>
                                <td><?=$this->M_mst_barang->getDetail($row->barangid)->nama;?></td>
                                <td><?=number_format($row->qty,0);?></td>
                                <td><?=number_format($row->harga,0);?></td>
                                <td><?=number_format($row->total,0);?></td><td>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <?=form_open("Laporan/LPenjualan/addDetail","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal" class="col-sm-4 control-label">Barang <?=$data->id?></label>
                                <input type="hidden" class="form-control" placeholder="penjualanid" name="penjualanid" value="<?php echo $data->id;?>">
                                <div class="col-sm-8">
                                    <select name="barangid" id="barangid" required class="form-control">
                                        <option value="">- Sparepart -</option>
                                        <?php foreach($rowBarang as $row):?>
                                            <option value="<?=$row->id?>" ><?=$row->nama?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga" class="col-sm-4 control-label">Harga Satuan</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="harga" placeholder="harga satuan" name="harga" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="qty" class="col-sm-4 control-label">Jumlah</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="qty" placeholder="jumlah" name="qty" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Laporan/LPenjualan/detailJson/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('#id').val(id).hide();
                $('#tanggal').val(data.tanggal);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#tanggal').val("");
    }
</script>