<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
    $role = $this->M_role->getDetail($user->roleid);
    $codename = $this->config->item("CODENAME");
    $sitename = $this->config->item("SITENAME");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$sitename?> | Dashboard</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  
  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/animate.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/style.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/style-responsive.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/theme/default.css" rel="stylesheet" id="theme" />
  <!-- ================== END BASE CSS STYLE ================== -->
  
  <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
  <link href="<?=base_url('extras/');?>plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
  <!-- ================== END PAGE LEVEL STYLE ================== -->
  
  <!-- ================== BEGIN BASE JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/pace/pace.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
  <!-- ================== END BASE JS ================== -->
  
</head>
<body><!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <!-- begin #page-container -->
  <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
      <!-- begin container-fluid -->
      <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
          <a href="<?=site_url('');?>" class="navbar-brand"><span class="navbar-logo"></span> <?=$sitename?></a>
          <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->
        
        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown navbar-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url('extras/');?>img/avatar5.png" alt="" /> 
              <span class="hidden-xs"><?=$user->fullname;?></span> <b class="caret"></b>
            </a>
            <ul class="dropdown-menu animated fadeInLeft">
              <li class="arrow"></li>
              <li><a href="javascript:;">Edit Profile</a></li>
              <li><a href="javascript:;">Log Out</a></li>
            </ul>
          </li>
        </ul>
        <!-- end header navigation right -->
      </div>
      <!-- end container-fluid -->
    </div>
    <!-- end #header -->
    
    <!-- begin #sidebar -->
    <div id="sidebar" class="sidebar">
      <!-- begin sidebar scrollbar -->
      <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
          <li class="nav-profile">
            <div class="image">
              <a href="javascript:;"><img src="<?=base_url('extras/');?>img/avatar5.png" alt="" /></a>
            </div>
            <div class="info">
              <?=$user->fullname;?>
              <small><?=$role->nama;?></small>
            </div>
          </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
          <li class="nav-header">Navigation</li>

          <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Home</span></a></li>
          <li class="<?=($this->uri->segment(2) == 'Kategori')?'active':''?>"><a href="<?=site_url('Master/Kategori')?>"><i class="fa fa-tag"></i> <span>Kategori</span></a></li>
          <li class="<?=($this->uri->segment(2) == 'Barang')?'active':''?>"><a href="<?=site_url('Master/Barang')?>"><i class="fa fa-archive"></i> <span>Barang</span></a></li>
          <li class="<?=($this->uri->segment(2) == 'Supplier')?'active':''?>"><a href="<?=site_url('Master/Supplier')?>"><i class="fa fa-truck"></i> <span>Supplier</span></a></li>
          <li class="<?=($this->uri->segment(2) == 'User')?'active':''?>"><a href="<?=site_url('Master/User')?>"><i class="fa fa-users"></i> <span>User</span></a></li>
          <li class="<?=($this->uri->segment(1) == 'Pembelian')?'active':''?>"><a href="<?=site_url('Pembelian')?>"><i class="fa fa-sign-in"></i> <span>Pembelian</span></a></li>
          <li class="<?=($this->uri->segment(1) == 'Penjualan')?'active':''?>"><a href="<?=site_url('Penjualan')?>"><i class="fa fa-sign-out"></i> <span>Penjualan</span></a></li>
          <li class="has-sub <?=($this->uri->segment(1) == 'Laporan')?'active':''?>">
            <a href="javascript:;">
                <b class="caret pull-right"></b>
                <i class="fa fa-files-o"></i>
                <span>Laporan</span>
              </a>
            <ul class="sub-menu">
                <li class="<?=($this->uri->segment(2) == 'LPenjualan')?'active':''?>"><a href="<?=site_url('Laporan/LPenjualan')?>">Penjualan</a></li>
                <li class="<?=($this->uri->segment(2) == 'LPembelian')?'active':''?>"><a href="<?=site_url('Laporan/LPembelian')?>">Pembelian</a></li>
                <li class="<?=($this->uri->segment(2) == 'LBarang')?'active':''?>"><a href="<?=site_url('Laporan/LBarang')?>">Barang</a></li>
            </ul>
          </li>
              <!-- begin sidebar minify button -->
          <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
              <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
      </div>
      <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->
    
    <!-- begin #content -->
    <div id="content" class="content">

    <!-- Main content -->
    <?php $this->load->view("page/".$konten);?>
    
    </div>
    <!-- end #content -->
    
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
  </div>
  <!-- end page container -->
  
  
<!-- ================== BEGIN BASE JS ================== -->
<script src="<?=base_url('extras/');?>plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
  <script src="<?=base_url('extras/');?>crossbrowserjs/html5shiv.js"></script>
  <script src="<?=base_url('extras/');?>crossbrowserjs/respond.min.js"></script>
  <script src="<?=base_url('extras/');?>crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?=base_url('extras/');?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

    <script src="<?=base_url('extras/');?>plugins/chart-js/chart.js"></script>
    <script src="<?=base_url('extras/');?>js/chart-js.demo.min.js"></script>
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?=base_url('extras/');?>plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.time.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.resize.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.pie.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/sparkline/jquery.sparkline.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?=base_url('extras/');?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?=base_url('extras/');?>js/dashboard.min.js"></script>
<script src="<?=base_url('extras/');?>js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
  
  <script>
    $(document).ready(function() {
      App.init();
      Dashboard.init();
      ChartJs.init();
    });
  </script>

</body>
</html>