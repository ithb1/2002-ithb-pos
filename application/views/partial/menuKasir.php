<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Home</span> </a></li>
  <li class="<?=($this->uri->segment(1) == 'Penjualan')?'active':''?>"><a href="<?=site_url('Penjualan')?>"><i class="fa fa-shopping-cart"></i> <span>Penjualan</span></a></li>
  <li class="<?=($this->uri->segment(2) == 'LPenjualan')?'active':''?>"><a href="<?=site_url('Laporan/LPenjualan')?>"><i class="fa fa-files-o"></i> <span>Laporan Penjualan</span></a></li>
</ul>