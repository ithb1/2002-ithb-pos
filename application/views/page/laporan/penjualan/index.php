
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Laporan Penjualan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href=""> Laporan Penjualan </a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Data Penjualan
                    </h3>
                    <div class="pull-right">
                        <a href="" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalSearch" onclick="clearForm()"><i class="fa fa-search"></i> FILTER</a>
                        <a href="#" class="btn btn-xs btn-success"><i class="fa fa-print"></i> Print</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nomor Faktur</th>
                            <th>Kasir</th>
                            <th>Nama Pembeli</th>
                            <th>No Telp Pembeli</th>
                            <th>Tanggal Faktur</th>
                            <th>Total</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($rowData as $row) :
                            ?>
                            <tr>
                                <td><?=$no++;?></td>
                                <td><?='#'.$row->nomorfaktur;?></td>
                                <td><?=$this->M_user->getDetail($row->userid)->fullname;?></td>
                                <td><?=$row->nama;?></td>
                                <td><?=$row->notelp;?></td>
                                <td><?=date("d-m-Y",strtotime($row->tanggal_faktur));?></td>
                                <td><?=number_format($row->total,0,",",".");?></td>
                                <td>
                                    <a href="<?=site_url('Laporan/LPenjualan/detail/'.$row->id);?>" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <?=form_open("Laporan/LPenjualan/add","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            ANDA YAKIN MENAMBAH ORDER?
<!--                            <div class="form-group">-->
<!--                                <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>-->
<!--                                <div class="col-sm-8">-->
<!--                                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">-->
<!--                                    <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal" value="">-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "ya","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="modalTanggal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <?=form_open("Laporan/LPenjualan/addTanggal","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal" class="col-sm-4 control-label">Tanggal</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                                    <input type="date" class="form-control" id="tanggal" placeholder="tanggal" name="tanggal_datang" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="modalSearch" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Filter</h4>
            </div>
            <?=form_open("Laporan/LPenjualan/search","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal_awal" class="col-sm-4 control-label">Tanggal Awal</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                                    <input type="date" class="form-control" id="tanggal_awal" placeholder="tanggal_awal" name="tanggal_awal" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tanggal_akhir" class="col-sm-4 control-label">Tanggal Akhir</label>
                                <div class="col-sm-8">
                                    <input type="hidden" class="form-control id" placeholder="id" name="id" value="">
                                    <input type="date" class="form-control" id="tanggal_akhir" placeholder="tanggal_akhir" name="tanggal_akhir" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <a href="<?=site_url('Laporan/LPenjualan/');?>" class="btn btn-default">reset</a>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>

<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Laporan/LPenjualan/detailJson/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('#id').val(id).hide();
                $('#tanggal').val(data.tanggal);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#tanggal').val("");
    }

    function tanggal(ini) {
        var id = $(ini).attr('data-id');
        $('.id').val(id);
    }
</script>