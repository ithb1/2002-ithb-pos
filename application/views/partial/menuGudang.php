<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Home</span> </a></li>
  <li class="<?=($this->uri->segment(2) == 'Kategori')?'active':''?>"><a href="<?=site_url('Master/Kategori')?>"><i class="fa fa-tag"></i> <span>Kategori</span></a></li>
  <li class="<?=($this->uri->segment(2) == 'Barang')?'active':''?>"><a href="<?=site_url('Master/Barang')?>"><i class="fa fa-archive"></i> <span>Barang</span></a></li>
  <li class="<?=($this->uri->segment(1) == 'Pembelian')?'active':''?>"><a href="<?=site_url('Pembelian')?>"><i class="fa fa-shopping-basket"></i> <span>Pembelian</span></a></li>

</ul>