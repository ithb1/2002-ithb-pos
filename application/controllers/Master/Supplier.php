<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	
	var $kelas = "Master/Supplier";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

	}

	public function index(){
		$data["rowData"] = $this->M_mst_supplier->getAll();
		$data['konten'] = "master/supplier/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_supplier->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
        $data["alamat"] = $this->input->post("alamat");
        $data["notelp"] = $this->input->post("notelp");

		if($id) 
			$this->M_mst_supplier->update($id,$data);
		else 
			$this->M_mst_supplier->add($data);

		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_supplier->delete($id);
		redirect($this->kelas);
	}
}
