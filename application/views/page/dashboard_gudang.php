<div class="row">
    <div class="col-lg-4 col-xs-6">
        <?php $this->load->view("partial/grafik/total_pembelian");?>
    </div>
    <div class="col-lg-4 col-xs-6">
        <?php $this->load->view("partial/grafik/total_inventory");?>
    </div>
    <section class="col-lg-4">
        <?php $this->load->view("partial/grafik/top_kategori");?>
    </section>
</div>