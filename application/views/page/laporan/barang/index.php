
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
     Barang
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href=""> Barang </a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">
            Data Barang
          </h3>
            <div class="pull-right">
                <a href="#" class="btn btn-xs btn-success"><i class="fa fa-print"></i> Print</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
                <th>Kategori</th>
                <th>Nama</th>
              <th>Stok</th>
              <th>Harga</th
              <th>action</th>
            </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($rowData as $row) :
              ?>
              <tr>
                <td><?=$no++;?></td>
                  <td><?=$this->M_mst_kategori->getDetail($row->kategoriid)->nama;?></td>
                  <td><?=$row->nama;?></td>
                <td><?=$row->stokakhir;?></td>
                <td><?=number_format($row->hargajual,0);?></td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Barang</h4>
      </div>
      ?>
      <div class="modal-body">

          <div class="box-body">
            <div class="row">
              <div class="col-md-12">            
                <div class="form-group">
                  <label for="nama" class="col-sm-4 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="hidden" class="form-control" id="id" placeholder="id" name="id" value="">
                    <input type="text" class="form-control" id="nama" placeholder="nama" name="nama" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="kategoriid" class="col-sm-4 control-label">Kategori</label>
                  <div class="col-sm-8">
                      <select name="kategoriid" id="kategoriid" required class="form-control">
                          <option value="">- Kategori -</option>
                          <?php foreach($rowKategori as $kategori):?>
                              <option value="<?=$kategori->id?>" ><?=$kategori->nama?></option>
                          <?php endforeach;?>
                      </select>
                  </div>
                </div>
                  <div class="form-group">
                  <label for="stokakhir" class="col-sm-4 control-label">Stok</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="stokakhir" placeholder="stokakhir" name="stokakhir" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="hargajual" class="col-sm-4 control-label">Harga Jual</label>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" id="hargajual" placeholder="hargajual" name="hargajual" value="">
                  </div>
                </div>
              </div>        
            </div>
          </div>
          <!-- /.box-footer -->
      </div>
      <div class="modal-footer">
        <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <?=form_close();?>
    </div>

  </div>
</div>


<script>
  function getDetail(ini) {
    var id = $(ini).attr('data-id');
    $.ajax({
      type: 'GET',
      success: function (data) {
        //Do stuff with the JSON data
          console.log(data);
         $('#id').val(id).hide();
         $('#nama').val(data.nama);
          $('#kategoriid').val(data.kategoriid);
         $('#stokakhir').val(data.stokakhir);
         $('#hargajual').val(data.hargajual);
        }
    });
  }

  function clearForm() {    
     $('#id').val("");
     $('#nama').val("");
      $('#kategoriid').val(data.kategoriid);
     $('#stokakhir').val("");
     $('#hargajual').val("");
  }
</script>