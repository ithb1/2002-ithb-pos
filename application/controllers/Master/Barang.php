<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	
	var $kelas = "Master/Barang";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}

	}

	public function index(){
		$data["rowData"] = $this->M_mst_barang->getAll();
		$data["rowKategori"] = $this->M_mst_kategori->getAll();
		$data['konten'] = "master/barang/index";
		$this->load->view('template',$data);
	}

	public function detail($id){
	    header('Content-Type: application/json');
		$rowData = $this->M_mst_barang->getDetail($id);
	    echo json_encode( $rowData );
	}

	public function add(){
		$id = $this->input->post("id");
		$data["nama"] = $this->input->post("nama");
        $data["kategoriid"] = $this->input->post("kategoriid");
        $data["stokakhir"] = $this->input->post("stokakhir");
		$data["hargajual"] = $this->input->post("hargajual");

		if($id) 
			$this->M_mst_barang->update($id,$data);
		else 
			$this->M_mst_barang->add($data);

		redirect($this->kelas);
	}

	public function delete($id){		
		$this->M_mst_barang->delete($id);
		redirect($this->kelas);
	}
}
