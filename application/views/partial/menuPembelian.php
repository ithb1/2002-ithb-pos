<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li class="<?=($this->uri->segment(1) == 'Welcome')?'active':''?>"><a href="<?=site_url('Welcome')?>"><i class="fa fa-home"></i> <span>Home</span> </a></li>
  <li class="<?=($this->uri->segment(1) == 'Penjualan')?'active':''?>"><a href="<?=site_url('Pembelian')?>"><i class="fa fa-shopping-basket"></i> <span>Pembelian</span></a></li>
  <li class="<?=($this->uri->segment(2) == 'LPembelian')?'active':''?>"><a href="<?=site_url('Laporan/LPembelian')?>"><i class="fa fa-files-o"></i>  <span>Laporan Pembelian</span></a></li>
</ul>