<?php
$rowBarang = $this->M_mst_barang->getAll();
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pembelian
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url('');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=site_url('Pembelian');?>"> Pembelian </a></li>
        <li><a href=""> Pembelian #<?=$data->nomorfaktur?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Tabel Data FAKTUR <?=$data->nomorfaktur?>
                    </h3>
                    <div class="pull-right">
                        <a href="#" class="btn btn-xs btn-success"><i class="fa fa-print"></i> Print</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Barang</th>
                            <th>Harga Satuan</th>
                            <th>Jumlah</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ($rowData as $row) :
                            ?>
                            <tr>
                                <td><?=$no++;?></td>
                                <td><?=$this->M_mst_barang->getDetail($row->barangid)->nama;?></td>
                                <td><?=number_format($row->hargasatuan,0);?></td>
                                <td><?=number_format($row->jumlah,0);?></td>
                                <td><?=number_format($row->total,0);?></td>

                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->






<!-- Modal -->
<div id="modalForm" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <?=form_open("Laporan/LPembelian/addDetail","class='form-horizontal'");
            ?>
            <div class="modal-body">

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="tanggal" class="col-sm-4 control-label">Barang <?=$data->id?></label>
                                <input type="hidden" class="form-control" placeholder="pembelianid" name="pembelianid" value="<?php echo $data->id;?>">
                                <div class="col-sm-8">
                                    <select name="barangid" id="barangid" required class="form-control">
                                        <option value="">- Sparepart -</option>
                                        <?php foreach($rowBarang as $row):?>
                                            <option value="<?=$row->id?>" ><?=$row->nama?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hargasatuan" class="col-sm-4 control-label">Harga Satuan</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="hargasatuan" placeholder="harga satuan" name="hargasatuan" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jumlah" class="col-sm-4 control-label">Jumlah</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="jumlah" placeholder="harga satuan" name="jumlah" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
            </div>
            <div class="modal-footer">
                <?=form_submit("btnsubmit", "save","class='btn btn-success'");?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <?=form_close();?>
        </div>

    </div>
</div>


<script>
    function getDetail(ini) {
        var id = $(ini).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "<?=base_url('');?>Laporan/LPembelian/detailJson/"+id,
            success: function (data) {
                //Do stuff with the JSON data
                console.log(data);
                $('#id').val(id).hide();
                $('#tanggal').val(data.tanggal);
            }
        });
    }

    function clearForm() {
        $('#id').val("");
        $('#tanggal').val("");
    }
</script>