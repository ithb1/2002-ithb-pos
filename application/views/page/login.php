<?php
$codename = $this->config->item("CODENAME");
$sitename = $this->config->item("SITENAME");
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.6/admin/html/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Feb 2015 05:19:44 GMT -->
<head>
  <meta charset="utf-8" />
  <title><?=$sitename?> | Log in</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  
  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="<?=base_url('extras/');?>plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/animate.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/style.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/style-responsive.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/theme/default.css" rel="stylesheet" id="theme" />
  <!-- ================== END BASE CSS STYLE ================== -->
</head>
<body>
  <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <div class="login-cover">
      <div class="login-cover-image"><img src="<?=base_url('extras/');?>img/login-bg/bg-1.jpg" data-id="login-cover-image" alt="" /></div>
      <div class="login-cover-bg"></div>
  </div>
  <!-- begin #page-container -->
  <div id="page-container" class="fade">
      <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated flipInX">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">
                    <span class="logo"></span> <?=$codename?>
                    <small><?=$sitename?></small>
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end brand -->
            <div class="login-content">              

              <?php if($this->session->userdata("error")):?>
              <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                Username / Password yang anda masukan salah
              </div>
              <?php
              $this->session->unset_userdata("error");
              endif;
              ?>

                <?=form_open("Login/index/","class='margin-bottom-0'");?>
                  <div class="form-group m-b-20">
                      <input type="text" class="form-control input-lg" placeholder="Username" name="username"  />
                  </div>
                  <div class="form-group m-b-20">
                      <input type="password" class="form-control input-lg" placeholder="Password" name="password" />
                  </div>
                  <div class="login-buttons">
                      <?=form_submit("btnsubmit","Sign in","class='btn btn-success btn-block btn-lg'");?>
                  </div>
                <?=form_close();?>
            </div>
        </div>
        <!-- end login -->
        
  </div>
  <!-- end page container -->
  
  <!-- ================== BEGIN BASE JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/jquery/jquery-1.9.1.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/bootstrap/js/bootstrap.min.js"></script>
  <!--[if lt IE 9]>
    <script src="<?=base_url('extras/');?>crossbrowserjs/html5shiv.js"></script>
    <script src="<?=base_url('extras/');?>crossbrowserjs/respond.min.js"></script>
    <script src="<?=base_url('extras/');?>crossbrowserjs/excanvas.min.js"></script>
  <![endif]-->
  <script src="<?=base_url('extras/');?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery-cookie/jquery.cookie.js"></script>
  <!-- ================== END BASE JS ================== -->
  
  <!-- ================== BEGIN PAGE LEVEL JS ================== -->
  <script src="<?=base_url('extras/');?>js/login-v2.demo.min.js"></script>
  <script src="<?=base_url('extras/');?>js/apps.min.js"></script>
  <!-- ================== END PAGE LEVEL JS ================== -->

  <script>
    $(document).ready(function() {
      App.init();
      LoginV2.init();
    });
  </script>
</body>

<!-- Mirrored from seantheme.com/color-admin-v1.6/admin/html/login_v2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Feb 2015 05:21:11 GMT -->
</html>

