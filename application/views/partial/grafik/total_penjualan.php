<div class="widget widget-stats bg-green">
	<div class="stats-icon"><i class="fa fa-dollar"></i></div>
	<div class="stats-info">
		<h4><b>Total Penjualan</b> -<?=date("M Y", strtotime($bulanFilter))?>-</h4>
		<p>Rp <?=number_format($totalRupiahPenjualan,0,',','.')?></p>	
	</div>
</div>