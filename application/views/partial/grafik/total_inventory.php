<div class="widget widget-stats bg-purple">
    <div class="stats-icon"><i class="fa fa-building"></i></div>
    <div class="stats-info">
        <h4>Total Inventory Saat Ini</h4>
        <p><?=$totalBarang?></p>  
        <hr>
        <table>
            <?php foreach ($rowKategori as $index => $row):
                $stok = $this->M_mst_barang->getByQuery("SELECT SUM(stokakhir) as stokakhir FROM mst_barang WHERE kategoriid = $row->id")[0];?>
                <tr>
                    <td><?=$row->nama?></td> <td>: <?=$stok->stokakhir ?? 0;?> unit</td>
                </tr>
            <?php endforeach;?>
        </table>
    </div>
</div>
