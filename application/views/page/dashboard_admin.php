
<div class="row">
    <div class="col-lg-6 col-xs-6">
        <?php $this->load->view("partial/grafik/faktur_penjualan");?>
    </div>
    <!-- ./col -->
    <div class="col-lg-6 col-xs-6">
        <?php $this->load->view("partial/grafik/total_penjualan");?>
    </div>
    <div class="col-lg-6 col-xs-6">
        <?php $this->load->view("partial/grafik/faktur_pembelian");?>
    </div>
    <!-- ./col -->
    <div class="col-lg-6 col-xs-6">
        <?php $this->load->view("partial/grafik/total_pembelian");?>
    </div>
    <!-- ./col -->
</div>

<!--    CHART-->
<div class="row">
    <section class="col-lg-12">
        <?php $this->load->view("partial/grafik/total_transaksi");?>
    </section>
</div>
<div class="row">
    <section class="col-lg-3">
        <?php $this->load->view("partial/grafik/total_inventory");?>
    </section>
    <section class="col-lg-6">
        <?php $this->load->view("partial/grafik/total_transaksi_weekly");?>
    </section>
    <section class="col-lg-3">
        <?php $this->load->view("partial/grafik/top_kategori");?>
    </section>
</div>