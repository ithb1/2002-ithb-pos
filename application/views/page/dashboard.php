
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
<li><a href="javascript:;">Home</a></li>
<li class="active">Dashboard</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Home <small>Dashboard</small></h1>
<!-- end page-header -->


<div class="row">
    <div class="col-md-6">
        <?=form_open("Welcome","class='form-horizontal'");?>
        <div class="col-md-3">
            <input type="month" name="bulanFilter" class="form-control" value="<?=$bulanFilter ?? date("Y-m")?>">
        </div>
        <div class="col-md-1">
            <?=form_submit("btnsubmit", "filter","class='btn btn-default'");?>
        </div>
        <?=form_close();?>
    </div>
</div>
<hr>
<?php
$this->load->view("partial/grafik/reminder");

if ($user->roleid == 1):
    $this->load->view("page/dashboard_admin");
elseif ($user->roleid == 2):
    $this->load->view("page/dashboard_kasir");
elseif ($user->roleid == 3):
    $this->load->view("page/dashboard_gudang");
elseif ($user->roleid == 4):
    $this->load->view("page/dashboard_pembelian");
endif;
?>
