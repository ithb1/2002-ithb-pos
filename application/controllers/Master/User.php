<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    var $kelas = "Master/User";

    function __construct(){
        parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Welcome");
        }
        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);
    }

    public function index(){
        $data["rowData"] = $this->M_user->getAll();
        $data["rowRole"] = $this->M_role->getAll();
        $data['konten'] = "master/user/index";
        $this->load->view('template',$data);
    }

    public function detail($id){
        header('Content-Type: application/json');
        $rowData = $this->M_user->getDetail($id);
        echo json_encode( $rowData );
    }

    public function add(){
        $id = $this->input->post("userid");
        $data["username"] = $this->input->post("username");
        $data["fullname"] = $this->input->post("fullname");
        $data["roleid"] = $this->input->post("roleid");
        $data["password"] = $this->input->post("password") ? $this->encrypt->encode($this->input->post("password")) : $this->encrypt->encode("asdasd");

        if($id){
            $this->M_user->update($id,$data);
        }
        else{
            $this->M_user->add($data);
        }

        redirect($this->kelas);
    }

    public function resetpassword($id)
    {
        $data["password"] = $this->encrypt->encode("asdasd");

        $this->M_user->update($id,$data);

        redirect($this->kelas);
    }

    public function delete($id){
        $user = $this->M_user->getDetail($id);
        $this->M_user->delete($id);
        redirect($this->kelas);
    }
}
