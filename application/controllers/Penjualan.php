<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

    var $kelas = "Penjualan";

    function __construct(){
        parent::__construct();
        if (!$this->session->userdata("id")){
            redirect("Login");
        }

        $id = $this->session->userdata("id");
        $this->user = $this->M_user->getDetail($id);

    }

    public function index(){
        $data["rowData"] = $this->M_penjualan->getAll();
        $data['konten'] = "penjualan/index";
        $this->load->view('template',$data);
    }

    public function detail($id){
        $data["data"] = $this->M_penjualan->getDetail($id);
        $data["rowData"] = $this->M_penjualan_detail->getAllBy("penjualanid = ".$id);
        $data['konten'] = "penjualan/detail";
        $this->load->view('template',$data);
    }

    public function detailJson($id){
        header('Content-Type: application/json');
        $rowData = $this->M_penjualan->getDetail($id);
        echo json_encode( $rowData );
    }

    public function add(){
        $id = $this->input->post("penjualanid");
        $data["userid"] = $this->user->userid;
        $data["nama"] = $this->input->post("nama");
        $data["notelp"] = $this->input->post("notelp");
        $data["tanggal_faktur"] = date("Y-m-d H:i:s");

        if($id)
            $this->M_penjualan->update($id,$data);
        else{
            $data["nomorfaktur"] = $this->nomor("FA");
            $this->M_penjualan->add($data);
            $id = $this->M_penjualan->getMax("id");
        }

        redirect($this->kelas."/detail/".$id);
    }

    public function addDetail(){
        $data["penjualanid"] = $penjualanid = $this->input->post("penjualanid");
        $data["barangid"] = $barangid = $this->input->post("barangid");
        $data["qty"] = $qty = $this->input->post("qty");
        $data["harga"] = $harga = $this->input->post("harga");
        $barang = $this->M_mst_barang->getDetail($barangid);

        if($barang->stokakhir < $qty){
            $this->session->set_flashdata("warning","Stok <strong>$barang->nama</strong> tidak mencukupi");
        }else{
//            tambah detail
            $data["total"] = $totalInput = $harga*$qty;
            $this->M_penjualan_detail->add($data);

//            update barang
            $this->M_mst_barang->update($barangid, array("stokakhir" => $barang->stokakhir - $qty));

//            update total
            $total = $this->M_penjualan->getDetail($penjualanid)->total;
            $dataPembelian["total"] = $total + $totalInput;
            $this->M_penjualan->update($penjualanid,$dataPembelian);
        }

        redirect($this->kelas."/detail/".$penjualanid);
    }

    public function save($id){
        // $id = $this->input->post("penjualanid");
        $rowDetailPenjualan = $this->M_penjualan_detail->getAllBy("penjualanid = ".$id);
        $total = 0;

        foreach ($rowDetailPenjualan as $row) {
            //update stok SPAREPART
            $total+=$row->total;
            $barang = $this->M_mst_barang->getDetail($row->sparepartid);
            $dataSparepart["stokakhir"]= $barang->stokakhir - $row->qty;
            $this->M_mst_barang->update($row->barangid,$dataSparepart);
        }

        //add FAKTUR
        $dataPenjualan["total"] = $total;
        $this->M_penjualan->update($id,$dataPenjualan);

        redirect($this->kelas);
    }

    public function update($id){
        if($this->input->post("btnsubmit")){
            $data["idrefatribut"] = $this->input->post("idrefatribut");
            $this->M_penjualan->update($id,$data);
            redirect($this->kelas);
        }
        $data["data"] = $this->M_penjualan->getDetail($id);
        $data['konten'] = "penjualan/index";
        $this->load->view('template',$data);
    }

    public function batal($id){
        $data["status"] = "NF";
        $this->M_penjualan->update($id,$data);
        redirect($this->kelas);
    }

    public function delete($id){
        $rowWorkorder = $this->M_penjualan_detail->getAllBy("penjualanid = $id");
        foreach ($rowWorkorder as $row) {
            $this->deleteDetail($row->detailwoid, 1);
        }

        $this->M_penjualan->delete($id);
        redirect($this->kelas);
    }

    public function deleteDetail($id, $loop = 0){
        $penjualanid = $this->M_penjualan_detail->getDetail($id)->penjualanid;
        $this->M_penjualan_detail->delete($id);
        if($loop != 1) redirect($this->kelas."/detail/".$penjualanid);
    }

    public function nomor($param){
        $max = $this->M_penjualan->getMax("id");
        $nomor = sprintf("%05d",$max+1);
        return $param.$nomor;
    }
}
