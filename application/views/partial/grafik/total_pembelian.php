<div class="widget widget-stats bg-red">
	<div class="stats-icon"><i class="fa fa-dollar"></i></div>
	<div class="stats-info">
		<h4><b>Total Pembelian</b> -<?=date("M Y", strtotime($bulanFilter))?>-</h4>
		<p>Rp <?=number_format($totalRupiahPembelian,0,',','.')?></p>	
	</div>
</div>