<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		 if (!$this->session->userdata("id")){
		 	redirect("Login");
		 }

		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);
	}

	public function index()
	{
	    $bulanFilter = $this->input->post("bulanFilter");
	    $tanggalFilter = $this->input->post("tanggalFilter");
	    $rowKrisis = [];
        $bulantahun = $bulanFilter ?? date('Y-m');
//        echo $bulantahun;die;
        $fakturPenjualan = $this->M_penjualan->getAllBy("tanggal_faktur LIKE '$bulantahun-%'");
		$fakturPembelian = $this->M_pembelian->getAllBy("tanggal LIKE '$bulantahun-%'");
		$totalStok = $this->M_mst_barang->getByQuery("SELECT SUM(stokakhir) as stokakhir FROM mst_barang");
		$totalRupiahPenjualan = $this->M_penjualan->getByQuery("SELECT SUM(total) as total FROM penjualan WHERE tanggal_faktur LIKE '$bulantahun-%'");
		$totalRupiahPembelian = $this->M_pembelian->getByQuery("SELECT SUM(total) as total FROM pembelian WHERE tanggal LIKE '$bulantahun-%'");
		$topSell = $this->M_penjualan->getByQuery("SELECT
                                                        p.id,
                                                        p.tanggal_faktur,
                                                        pd.barangid,
	                                                    b.nama namabarang,
                                                        SUM(pd.qty) as qty
                                                    FROM
                                                        penjualan p
                                                        LEFT JOIN penjualan_detail pd ON p.id = pd.penjualanid
                                                        LEFT JOIN mst_barang b ON b.id = pd.barangid
                                                    WHERE
                                                        p.tanggal_faktur LIKE '$bulantahun-%' 
                                                    GROUP BY pd.barangid
                                                    ORDER BY qty DESC LIMIT 5");
		$bottomSell = $this->M_penjualan->getByQuery("SELECT
                                                        p.id,
                                                        p.tanggal_faktur,
                                                        pd.barangid,
	                                                    b.nama namabarang,
                                                        SUM(pd.qty) as qty
                                                    FROM
                                                        penjualan p
                                                        LEFT JOIN penjualan_detail pd ON p.id = pd.penjualanid
                                                        LEFT JOIN mst_barang b ON b.id = pd.barangid
                                                    WHERE
                                                        p.tanggal_faktur LIKE '$bulantahun-%' 
                                                    GROUP BY pd.barangid
                                                    ORDER BY qty ASC LIMIT 5");

        $barangKrisis = $this->M_mst_barang->getAllBy("stokakhir <= 5");
        foreach ($barangKrisis as $item) {
            $krisis = array(
                "nama" => $item->nama,
                "sisa" => $item->stokakhir
            );
            array_push($rowKrisis, $krisis);
        }

		$data["totalFakturPenjualan"] = count($fakturPenjualan);
		$data["totalFakturPembelian"] = count($fakturPembelian);
		$data["totalBarang"] = $totalStok[0]->stokakhir;
		$data["totalRupiahPenjualan"] = $totalRupiahPenjualan[0]->total;
		$data["totalRupiahPembelian"] = $totalRupiahPembelian[0]->total;
		$data["topSell"] = $topSell;
		$data["bottomSell"] = $bottomSell;
		$data["rowKrisis"] = $rowKrisis;
		$data["user"] = $this->user;
		$data["tanggalFilter"] = $tanggalFilter;
		$data["bulanFilter"] = $bulantahun;
		$data["target"] = 20;
		$data["rowKategori"] = $this->M_mst_kategori->getAll();
		$konten = "dashboard";

        $data["konten"] = $konten;
		$this->load->view('template',$data);
	}

	public function index2()
	{
	    $bulanFilter = $this->input->post("bulanFilter");
	    $tanggalFilter = $this->input->post("tanggalFilter");
	    $rowKrisis = [];
        $bulantahun = $bulanFilter ?? date('Y-m');
//        echo $bulantahun;die;
        $fakturPenjualan = $this->M_penjualan->getAllBy("tanggal_faktur LIKE '$bulantahun-%'");
		$fakturPembelian = $this->M_pembelian->getAllBy("tanggal LIKE '$bulantahun-%'");
		$totalStok = $this->M_mst_barang->getByQuery("SELECT SUM(stokakhir) as stokakhir FROM mst_barang");
		$totalRupiahPenjualan = $this->M_penjualan->getByQuery("SELECT SUM(total) as total FROM penjualan WHERE tanggal_faktur LIKE '$bulantahun-%'");
		$totalRupiahPembelian = $this->M_pembelian->getByQuery("SELECT SUM(total) as total FROM pembelian WHERE tanggal LIKE '$bulantahun-%'");
		$topSell = $this->M_penjualan->getByQuery("SELECT
                                                        p.id,
                                                        p.tanggal_faktur,
                                                        pd.barangid,
	                                                    b.nama namabarang,
                                                        SUM(pd.qty) as qty
                                                    FROM
                                                        penjualan p
                                                        LEFT JOIN penjualan_detail pd ON p.id = pd.penjualanid
                                                        LEFT JOIN mst_barang b ON b.id = pd.barangid
                                                    WHERE
                                                        p.tanggal_faktur LIKE '$bulantahun-%' 
                                                    GROUP BY pd.barangid
                                                    ORDER BY qty DESC LIMIT 5");
		$bottomSell = $this->M_penjualan->getByQuery("SELECT
                                                        p.id,
                                                        p.tanggal_faktur,
                                                        pd.barangid,
	                                                    b.nama namabarang,
                                                        SUM(pd.qty) as qty
                                                    FROM
                                                        penjualan p
                                                        LEFT JOIN penjualan_detail pd ON p.id = pd.penjualanid
                                                        LEFT JOIN mst_barang b ON b.id = pd.barangid
                                                    WHERE
                                                        p.tanggal_faktur LIKE '$bulantahun-%' 
                                                    GROUP BY pd.barangid
                                                    ORDER BY qty ASC LIMIT 5");

        $barangKrisis = $this->M_mst_barang->getAllBy("stokakhir <= 5");
        foreach ($barangKrisis as $item) {
            $krisis = array(
                "nama" => $item->nama,
                "sisa" => $item->stokakhir
            );
            array_push($rowKrisis, $krisis);
        }

		$data["totalFakturPenjualan"] = count($fakturPenjualan);
		$data["totalFakturPembelian"] = count($fakturPembelian);
		$data["totalBarang"] = $totalStok[0]->stokakhir;
		$data["totalRupiahPenjualan"] = $totalRupiahPenjualan[0]->total;
		$data["totalRupiahPembelian"] = $totalRupiahPembelian[0]->total;
		$data["topSell"] = $topSell;
		$data["bottomSell"] = $bottomSell;
		$data["rowKrisis"] = $rowKrisis;
		$data["user"] = $this->user;
		$data["tanggalFilter"] = $tanggalFilter;
		$data["bulanFilter"] = $bulantahun;
		$data["target"] = 20;
		$data["rowKategori"] = $this->M_mst_kategori->getAll();
		$konten = "dashboard";

        $data["konten"] = $konten;
		$this->load->view('template2',$data);
	}

	public function profil(){
	    if($this->input->post("btnsubmit")){
			$data["nama"] = $this->input->post("nama");
			if($this->input->post("password")){
				$data["password"] = $this->encrypt->encode($this->input->post("password"));
			}
			$this->M_user->update($this->user->userid,$data);
			redirect("Welcome/profil");
	    }
		
		$data['user'] = $this->user;
		$data['konten'] = "profil";
		$this->load->view('template',$data);
	}

	public function export(){
        $data['filename'] = "Laporan";
		$data["rowData"] = $this->M_user->getAll();
        $data['konten'] = "page/export/user";
        $this->load->view("page/export/templatePdf",$data);
	}
}
